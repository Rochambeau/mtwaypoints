package.cpath = package.cpath .. ";/usr/share/lua/5.2/?.so"
package.path = package.path .. ";/usr/share/zbstudio/lualibs/mobdebug/?.lua"
require('mobdebug').start()

waypoints = {}

waypoints_table = {} --ipairs
local selected


-------------------------------------------------------------------------------
-- set waypoint
-------------------------------------------------------------------------------
function waypoints.set_waypoint(player_name, wp_name, wp_show, wp_pos)
  local wp_table = {}  
  -- get player pos
  local player = minetest.get_player_by_name(player_name)
  --
  local pos
  if wp_pos == nil 
  then
    pos = player:get_pos() -- new waypoint
  else
    pos = wp_pos -- toggle waypoint
  end
-- get player meta
  local meta = player:get_meta()
  --
  -- check if waypoint name is already taken
  --
  local wp_exists = meta:get_string("wp_"..wp_name)
  if wp_exists ~= ""
  then
    minetest.chat_send_player(player_name, "Waypoint " .. wp_name .." alredy exists.")
    return
  end
  -- add waypoint to hud 
  local index
  if wp_show == "y" 
  then
    index = player:hud_add({
        hud_elem_type = "waypoint",
        number = 0xffffff, -- color according to age of bones.
        name = wp_name, --.. minetest.pos_to_string(pos),
        text = " nodes away",
        world_pos = pos
      })
  end
  -- add waypoint to table attribute
  wp_table = {
    id = index,
    label = wp_name, 
    show = wp_show,
    coords = pos
  }
  wp_table_string = minetest.serialize({wp_table})
  meta:set_string("wp_"..wp_name, wp_table_string)

  -- add waypoint to internal list
  waypoints_table[#waypoints_table+1] = wp_table
  --sort table ascending by lable
  table.sort(waypoints_table, function(a,b) return a.label < b.label end)

end
-------------------------------------------------------------------------------
-- toggle waypoint
-------------------------------------------------------------------------------
function waypoints.toggle_waypoint(player_name, wp_name)
  if wp_name == ""
  then
    minetest.chat_send_player(player_name, "Waypoint name empty")
    return --todo notify player to give a waypoint name
  end
  local wp_table = {}  
  local old_wp_table = {}  
  -- get player pos
  local player = minetest.get_player_by_name(player_name)
  -- get player meta
  local meta = player:get_meta()
  --
  -- check if waypoint name is already taken
  --
  local wp_exists = meta:get_string("wp_"..wp_name)
  -- save existing waypoint, toggle show value, delete and rebuild
  if wp_exists ~= ""
  then
    --save old wp
    old_wp_table = minetest.deserialize(wp_exists)[1]
    --delete old wp
    waypoints.del_waypoint(player_name, wp_name)
    -- toggle show/hide
    local show
    if old_wp_table.show == "y"
    then
      show = "n"
    else
      show = "y"
    end
    --set new wp
    waypoints.set_waypoint(player_name, wp_name, show, old_wp_table.coords)
  end

end
-------------------------------------------------------------------------------
-- delete waypoint
-------------------------------------------------------------------------------
function waypoints.del_waypoint(player_name, wp_name)
  -- remove from player attributes
  local player = minetest.get_player_by_name(player_name)
  local meta = player:get_meta()
  meta:set_string("wp_"..wp_name, "")
  for i = 1, #waypoints_table do
    if waypoints_table[i].label == wp_name
    then
      -- remove from hud
      local id = waypoints_table[i].id
      player:hud_remove(id)
      -- remove from waypoints_table
      table.remove(waypoints_table, i)
      break
    end
  end
end
-------------------------------------------------------------------------------
-- load waypoints from player attributes
-------------------------------------------------------------------------------
function waypoints.loadwaypoints(player)
  local meta = player:get_meta()
  local player_attributes = meta:to_table().fields
  local ix = 1
  for i, value in pairs(player_attributes) do
    if string.find(i,"wp_")
    then
      local value_deserialized = minetest.deserialize(value)[1]
      waypoints_table[ix] = value_deserialized
      ix = ix + 1
    end
  end
  --sort table ascending by lable
  table.sort(waypoints_table, function(a,b) return a.label < b.label end)
end
-------------------------------------------------------------------------------
-- step throught waypoints_table and show waypoints on hud
-------------------------------------------------------------------------------
function waypoints.create_hud(player)
  local waypoints_table_new = {}
  for i = 1, #waypoints_table do
    local index
    if waypoints_table[i].show == 'y' then
      index = player:hud_add({
          hud_elem_type = "waypoint",
          number = 0xFFffff, -- color according to age of bones.
          name = waypoints_table[i].label, --.. minetest.pos_to_string(pos),
          text = " nodes away",
          world_pos = waypoints_table[i].coords
        })
    end

    -- add waypoint to table
    wp_table = {
      id = index,
      label = waypoints_table[i].label, 
      show = waypoints_table[i].show,
      coords = waypoints_table[i].coords
    }
    waypoints_table_new[i] = wp_table

  end

  return waypoints_table_new
end
-------------------------------------------------------------------------------
-- look for waypoints and show them or not
-------------------------------------------------------------------------------
minetest.register_on_joinplayer(function(player)
    waypoints.loadwaypoints(player)
    waypoints_table = waypoints.create_hud(player)
  end)
-------------------------------------------------------------------------------
-- formspec
-------------------------------------------------------------------------------
local function show_formspec(name, page, data)
  local f = ""
  f = "size[12 ,10]" ..
  "textlist[0,0;8,10;list;"
  if #waypoints_table == 0
  then
    f = f.."," -- empty list
  else
    for i = 1, #waypoints_table do
      local color
      if waypoints_table[i].show == 'y'
      then
        color = ""
      else
        color = "#C0C0C0"
      end
      f = f..color..waypoints_table[i].label..","
    end
  end
  f = f:sub(1, -2).. -- Cut away the last ",".
  "]"..
  "container[9,0]"..--]]
  "field[0,0.5;3,1;name;Name;"..((data and tostring(data.name)) or "").."]"..
  "button[0,1;2,1;add;Add]"..    
  "button[0,2;2,1;toggle;Toggle]"..    
  "button[0,3;2,1;delete;Delete]"..
  "button_exit[0,9;2,1;exit;Exit]"..
  "container_end[]"
  minetest.show_formspec(name, "waypoints_"..page, f)

end
-------------------------------------------------------------------------------
-- react to formspec input
-------------------------------------------------------------------------------
minetest.register_on_player_receive_fields(function(player, formname, fields)
    if formname:sub(1, 9) ~= "waypoints" then
      return
    end
    local page = formname:sub(11)
    --~ print(dump(fields))
    if fields.add then
      local player_name = player:get_player_name()
      waypoints.set_waypoint(player_name, fields.name, "y")
      show_formspec(player:get_player_name(), "main")
    elseif fields.delete then
      local player_name = player:get_player_name()
      waypoints.del_waypoint(player_name, fields.name)
      show_formspec(player:get_player_name(), "main")
    elseif fields.toggle then
      local player_name = player:get_player_name()
      waypoints.toggle_waypoint(player_name, fields.name)
      show_formspec(player:get_player_name(), "main")
    elseif fields.list then
      if fields.list:sub(1, 4) == "CHG:" or
      fields.list:sub(1, 4) == "DCL:"
      then
        selected = tonumber(fields.list:sub(5))
      end
--      local number = 1
      for i = 1, #waypoints_table do
        if i == selected 
        then
          fields.name = waypoints_table[i].label 
          break
        end
--        number = number + 1
      end
      if fields.list:sub(1, 4) == "DCL:"
      then
        local player_name = player:get_player_name()
        waypoints.toggle_waypoint(player_name, fields.name)
      end
      show_formspec(player:get_player_name(), "main",  fields)
    end
    return true
    end)--]]


-------------------------------------------------------------------------------
-- define waypoints tool
-------------------------------------------------------------------------------
    minetest.register_craftitem("waypoints:tool", {
        description = "waypoint tool",
        inventory_image = "default_tool_woodshovel.png",
        on_use = function(itemstack, placer, pointed_thing)
          show_formspec(placer:get_player_name(), "main")
        end,
        --
        -- no function yet
        --
        on_place = function(itemstack, placer, pointed_thing)
          local a = 1

        end
      })